<?php
/**
 * GroceryCrud Library extended with a certain additional features / functionalities that empowers users to do more
 * with less coding. GroceryCrud itself is powerful. This library is just going to add up addon hands / tools over 
 * the same.
 * Copyright (C) 2016 - 2017  Amit Shah.
 *
 * @package    	power grocery CRUD
 * @copyright  	Copyright (c) 2010 through 2016, Amit Shah
 * @version     1.1
 * @author     	Amit Shah <amitbs@gmail.com>
 */

require_once APPPATH . "libraries/Grocery_CRUD.php";

class Power_Grocery_CRUD extends Grocery_CRUD {
	//Add up New Variables around here that will work along with the existing GroceryCrud System

	/**
	 * [$field_tips is a collection of array storing tips for each fields]
	 * @var array
	 */
	protected $field_tips							= array();

	/**
	 * [$field_mask Allows users to set the masks on the fields]
	 * @var null
	 */
	protected $field_mask							= null;

	/**
	 * [$footer Allows the user to set the footer on the list page / section]
	 * @var null
	 */
	protected $footer 								= null;
	protected $footer_style 						= null;

	/**
	 * By default set to false - but this variable holds the option / opportunity for user to copy a row
	 * @var boolean
	 */
	protected $enable_copy							= false;

	/**
	 * Holds the default values set by the user while showing add form
	 * @var array
	 */
	protected $field_default_values 				= array();

	/**
	 * Now fields for in regards with grouping
	 * @var boolean
	 */
	protected $is_grouped			= false;
	protected $groups				= array();
	protected $group_fields			= array();

	const COPY_ROW = 20;

	//The Default theme to be set to powerflexigrid so it can resume the same features of flexigrid and
	//also harness the pwer of the new features
	function _initialize_variables() {
		//Here - we also update the state array to add copy in case if it dose not exists
		if(!array_key_exists(20, $this->states)) {
			$this->states[20] = 'copy';
		}
		parent::_initialize_variables();
		$this->set_theme('powerflexigrid');
	}


	/**
	 *
	 * Sets tips to the given field that will be displayed to the right of the field
	 * @param string $field
	 * @param string $tip
	 */
	public function field_tip($field , $tip)
	{
		$this->field_tips[$field] = $tip;
		return $this;
	}

	/**
	 *
	 * Sets mask to the given field
	 * @param string $field
	 * @param string $mask
	 */
	public function mask_field($field , $mask)
	{
		$this->field_mask[$field] = $mask;
		return $this;
	}

	/**
	 * Sets the footer that needs to be displayed 
	 * @param String _footer - the footer content
	 */
	public function set_footer($_footer) {
		$this->footer = $_footer;
	}

	/**
	 * Allows the user to set the style for the footer
	 * @param String $footer_style - The footer style code
	 */
	public function set_footer_style($footer_style) {
		$this->footer_style = $footer_style;
	}

	/**
	 * Adds new group in selection
	 * @param String $group - Group Name
	 * @param String $title - Title of the group to be displayed
	 */
	function add_group($group, $title) {
		if(array_key_exists($group, $this->groups) !== FALSE) {
			$this->groups[$group] = $title;
		}
	}

	/**
	 * Allows the user to add a certain set of fields to the group
	 * @param String $group - Name of the group to which the fields are to be added
	 * @param String $field - Name of field to be added to the group
	 */
	function add_group_field($group, $field) {
		if(array_key_exists($group, $this->groups) !== FALSE) {
			if(array_search($field, $this->group_fields[$group]) === FALSE) {
				array_push($this->group_fields[$group], $field);
			}
		} else {
			$this->groups[$group] = $group;
			array_push($groups[$group], $field);
		}
	}

	/**
	 * Just another easier alternative for allowing the user to set the groups
	 * @param String $group        - Name of the group
	 * @param String $group_title  - Title of the group to be displayed
	 * @param String $group_fields - Set of fiels to be appended to the group
	 */
	public function set_group($group, $group_title, $group_fields)
	{
		$this->is_grouped = true;
		foreach ($group_fields as $field) {
			if(array_key_exists($group, $this->groups) !== FALSE) {
				if(array_search($field, $this->group_fields[$group]) === FALSE) {
					array_push($this->group_fields[$group], $field);
				}
			} else {
				$this->groups[$group] = $group_title;
				$this->group_fields[$group] = array();
				array_push($this->group_fields[$group], $field);
			}
		}
		return $this;
	}

	/**
	 * Re Write the logic to handle copy state as the same is not available.
	 * @return [type] [description]
	 */
	protected function getStateCode()
	{
		$state_string = $this->get_state_info_from_url()->operation;
		if($state_string == 'copy') {
			$state_code = $this::COPY_ROW;
		} else if( $state_string != 'unknown' && in_array( $state_string, $this->states ) )
			$state_code =  array_search($state_string, $this->states);
		else
			$state_code = 0;

		return $state_code;
	}

	//Added by Amit Shah for default field values
	//I will definately like to credit EZGone - for providing in such wonderful feature. Because of it
	//was able to add this up in power crud as default feature and also
	//was able to extend the functionality to allow user to copy the record
	/**	 * Helper Function
	 *
	 * Created by Ez (ezgoen@gmail.com) - email me for my real identity
	 *
	 */
	protected function get_field_default_values($field){
		return array_key_exists($field,$this->field_default_values) ? $this->field_default_values[$field] : null;
	}

	/**
	 * Use this function exactly the same way you would use field_type()
	 * see : http://www.grocerycrud.com/documentation/options_functions/field_type
	 *
	 * Originally Created by Ez (ezgoen@gmail.com) - email me for my real identity
	 *
	 * @explanation:
	 *				in the case of an ADD data form I want to provide default values to the fields.
	 *				I may also want to make the fields with default values readonly or completely hidden
	 *				The parent object does not allow/facilitate addidtion of default values to visible 
	 *				fields other than:
	 *					hidden
	 *					invisible
	 *					password
	 *					enum
	 *					set
	 *					dropdown
	 *					multiselect
	 *
	 *				I havent fooled around with invisible - so I cant comment on anything to do with that.
	 *				however I want my other fields - most commonly string and integer to have default values
	 *				additionally I may also want thos values to either editable OR readonly and visible at 
	 *				the same time AND I want my default values (if they are readonly) to go into the database
	 *				
	 */
	public function field_set_defaults($field,$value){
		  $this->field_default_values[$field]=$value;
	}

	function enable_copy() {
		$this->enable_copy = true;
	}

	function disable_copy() {
		$this->enable_copy = false;
	}

	/**
	 * Like above, since we dont want to disturb the parent functionality by altering it, we 
	 * override even this function to get the proper state in case the copy function is to be handled
	 * with.
	 * @return [type] [description]
	 */
	public function getStateInfo()
    {
    	$state_code = $this->getStateCode();
        $segment_object = $this->get_state_info_from_url();

        $first_parameter = $segment_object->first_parameter;
        $second_parameter = $segment_object->second_parameter;

        $state_info = (object)array();
        if($state_code == $this::COPY_ROW) {
        	if($first_parameter !== null)
			{
				$state_info = (object)array('primary_key' => $first_parameter);
			}
			else
			{
				throw new Exception('On the state "copy" the Primary key cannot be null', 6);
				die();
			}
			return $state_info;
        } else {
        	return parent::getStateInfo();
        }
    }

    /**
     * Get the current model
     * @return [type] GCModel - Default or Defined
     */
    public function getModel() {
		if($this->basic_model === null)
	   		$this->set_default_Model();
	  	return $this->basic_model;
	}

	protected function get_add_values() {
		$values = $this->basic_model->get_add_values();
	  	return $values;
	}

	function render() {
		$this->pre_render();
		if( $this->state_code != 0 )
		{
			$this->state_info = $this->getStateInfo();
		}
		else
		{
			throw new Exception('The state is unknown , I don\'t know what I will do with your data!', 4);
			die();
		}
		if($this->state_code == $this::COPY_ROW) :
			if($this->unset_add)
			{
				throw new Exception('You don\'t have permissions for this operation', 14);
				die();
			}

			if($this->theme === null)
				$this->set_theme($this->default_theme);
			$this->setThemeBasics();

			$this->set_basic_Layout();

			//Set the default values
			$state_info = $this->getStateInfo();
			$this->copy_row($state_info);

			$this->showAddForm();
			return $this->get_layout();
		else:
			return parent::render();
		endif;
	}

	protected function copy_row($state_info) {
		$fields = $this->get_add_fields();
		$types 	= $this->get_field_types();

		$field_values = $this->get_edit_values($state_info->primary_key);
		foreach($fields as $field)
		{
			$fieldName = $field->field_name;
			if(property_exists($field_values,$fieldName)) {
				if($types[$fieldName]->crud_type == 'upload_file') {
					//Make a copy of the same and then assign the new value to the same
					$oldFileName = $field_values->{$fieldName};
					$oldFileName = substr($oldFileName, 6);
					$newFileName = substr(uniqid(),-5).'-'. $oldFileName;
					$sourcePath = $types[$fieldName]->extras->upload_path . DIRECTORY_SEPARATOR . $field_values->{$fieldName};
					$newPath = $types[$fieldName]->extras->upload_path . DIRECTORY_SEPARATOR . $newFileName;
					if(is_file($sourcePath)) {
						copy($sourcePath, $newPath);
						$this->getModel()->set_add_value($field->field_name, $newFileName);
					}
				} else {
					$this->getModel()->set_add_value($field->field_name, $field_values->{$fieldName});
				}
			}
		}
	}

	protected function showList($ajax = false, $state_info = null)
	{
		$data = $this->get_common_data();

		$data->order_by 	= $this->order_by;

		$data->types 		= $this->get_field_types();

		$data->list = $this->get_list();
		$data->list = $this->change_list($data->list , $data->types);
		$data->list = $this->change_list_add_actions($data->list);

		$data->total_results = $this->get_total_results();

		$data->columns 				= $this->get_columns();

		$data->success_message		= $this->get_success_message_at_list($state_info);

		$data->primary_key 			= $this->get_primary_key();
		$data->add_url				= $this->getAddUrl();
		$data->edit_url				= $this->getEditUrl();
		$data->delete_url			= $this->getDeleteUrl();
        $data->delete_multiple_url	= $this->getDeleteMultipleUrl();
		$data->read_url				= $this->getReadUrl();
		$data->ajax_list_url		= $this->getAjaxListUrl();
		$data->ajax_list_info_url	= $this->getAjaxListInfoUrl();
		$data->export_url			= $this->getExportToExcelUrl();
		$data->print_url			= $this->getPrintUrl();
		$data->actions				= $this->actions;
		$data->unique_hash			= $this->get_method_hash();
		$data->order_by				= $this->order_by;

		$data->unset_add			= $this->unset_add;
		$data->unset_edit			= $this->unset_edit;
		$data->unset_read			= $this->unset_read;
		$data->unset_delete			= $this->unset_delete;
		$data->unset_export			= $this->unset_export;
		$data->unset_print			= $this->unset_print;

		//Added by Amit Shah - to set the footer & its style
		$data->footer				= $this->footer;
		$data->footer_style			= $this->footer_style;

		//Share the users option to enable / disable copy 
		$data->enable_copy			= $this->enable_copy;
		$data->copy_url				= $this->getCopyUrl();

		$default_per_page = $this->config->default_per_page;
		$data->paging_options = $this->config->paging_options;
		$data->default_per_page		= is_numeric($default_per_page) && $default_per_page >1 && in_array($default_per_page,$data->paging_options)? $default_per_page : 25;

		if($data->list === false)
		{
			throw new Exception('It is impossible to get data. Please check your model and try again.', 13);
			$data->list = array();
		}

		foreach($data->list as $num_row => $row)
		{
            $data->list[$num_row]->primary_key_value = $row->{$data->primary_key};
			$data->list[$num_row]->edit_url = $data->edit_url.'/'.$row->{$data->primary_key};
			$data->list[$num_row]->delete_url = $data->delete_url.'/'.$row->{$data->primary_key};
			$data->list[$num_row]->read_url = $data->read_url.'/'.$row->{$data->primary_key};
			$data->list[$num_row]->copy_url = $data->copy_url.'/'.$row->{$data->primary_key};
		}

		if(!$ajax)
		{
			$this->_add_js_vars(array('dialog_forms' => $this->config->dialog_forms));

			$data->list_view = $this->_theme_view('list.php',$data,true);
			$this->_theme_view('list_template.php',$data);
		}
		else
		{
			$this->set_echo_and_die();
			$this->_theme_view('list.php',$data);
		}
	}

	/**
	 * [showAddForm It is the similar functionality from GroceryCrud - Here.. we are just overriding the same so we can add up our special set of functionalities along with the add form]
	 */
	protected function showAddForm()
	{
		$this->set_js_lib($this->default_javascript_path.'/'.grocery_CRUD::JQUERY);

		$data 				= $this->get_common_data();
		$data->types 		= $this->get_field_types();
		//Here we add the new field tips that we are going to display against defined field in the form
		$data->tips			= $this->field_tips;
		//Here we add the definitions of field masks
		$data->masks		= $this->field_mask;
		//retrieves the add values that will be populated in the add form. This is useful only in case
		//user wana set his / her own default values for add form or copy the row
		$data->field_values = $this->get_add_values();

		//Add the group details
		$data->has_groups		= $this->is_grouped;
		$data->groups			= $this->groups;
		$data->group_fields		= $this->group_fields;

		$data->list_url 		= $this->getListUrl();
		$data->insert_url		= $this->getInsertUrl();
		$data->validation_url	= $this->getValidationInsertUrl();
		//$data->input_fields 	= $this->get_add_input_fields();
		//here we alter the default add input fields for the purpose of setting default values
		$data->input_fields  = $this->get_add_input_fields($data->field_values);

		$data->fields 			= $this->get_add_fields();
		$data->hidden_fields	= $this->get_add_hidden_fields();
		$data->unset_back_to_list	= $this->unset_back_to_list;
		$data->unique_hash			= $this->get_method_hash();
		$data->is_ajax 			= $this->_is_ajax();

		$this->_theme_view('add.php',$data);

		$this->_inline_js("var js_date_format = '".$this->js_date_format."';");

		$this->_get_ajax_results();
	}

	/**
	 * [showEditForm It is the similar functionality from GroceryCrud - Here.. we are just overriding the same so we can add up our special set of functionalities along with the edit form]
	 * @param  [type] $state_info [description]
	 */
	protected function showEditForm($state_info)
	{
		$this->set_js_lib($this->default_javascript_path.'/'.grocery_CRUD::JQUERY);

		$data 				= $this->get_common_data();
		$data->types 		= $this->get_field_types();
		//Here we add the new field tips that we are going to display against defined field in the form
		$data->tips			= $this->field_tips;
		//Here we add the definitions of field masks
		$data->masks		= $this->field_mask;

		//Add the group details
		$data->has_groups		= $this->is_grouped;
		$data->groups			= $this->groups;
		$data->group_fields		= $this->group_fields;

		$data->field_values = $this->get_edit_values($state_info->primary_key);

		$data->add_url		= $this->getAddUrl();

		$data->list_url 	= $this->getListUrl();
		$data->update_url	= $this->getUpdateUrl($state_info);
		$data->delete_url	= $this->getDeleteUrl($state_info);
		$data->read_url		= $this->getReadUrl($state_info->primary_key);
		$data->input_fields = $this->get_edit_input_fields($data->field_values);
		$data->unique_hash			= $this->get_method_hash();

		$data->fields 		= $this->get_edit_fields();
		$data->hidden_fields	= $this->get_edit_hidden_fields();
		$data->unset_back_to_list	= $this->unset_back_to_list;

		$data->validation_url	= $this->getValidationUpdateUrl($state_info->primary_key);
		$data->is_ajax 			= $this->_is_ajax();

		$this->_theme_view('edit.php',$data);
		
		$this->_inline_js("var js_date_format = '".$this->js_date_format."';");

		$this->_get_ajax_results();
	}

	/**
	 * [showReadForm It is the similar functionality from GroceryCrud - Here.. we are just overriding the same so we can add up our special set of functionalities along with the read form]
	 * @param  [type] $state_info
	 */
	protected function showReadForm($state_info)
	{
		$this->set_js_lib($this->default_javascript_path.'/'.grocery_CRUD::JQUERY);

		$data 				= $this->get_common_data();
		$data->types 		= $this->get_field_types();
		//Here we add the new field tips that we are going to display against defined field in the form
		$data->tips			= $this->field_tips;
		//Here we add the definitions of field masks
		$data->masks		= $this->field_mask;

		//Add the group details
		$data->has_groups		= $this->is_grouped;
		$data->groups			= $this->groups;
		$data->group_fields		= $this->group_fields;

		$data->field_values = $this->get_edit_values($state_info->primary_key);

		$data->add_url		= $this->getAddUrl();

		$data->list_url 	= $this->getListUrl();
		$data->update_url	= $this->getUpdateUrl($state_info);
		$data->delete_url	= $this->getDeleteUrl($state_info);
		$data->read_url		= $this->getReadUrl($state_info->primary_key);
		$data->input_fields = $this->get_read_input_fields($data->field_values);
		$data->unique_hash			= $this->get_method_hash();

		$data->fields 		= $this->get_read_fields();
		$data->hidden_fields	= $this->get_edit_hidden_fields();
		$data->unset_back_to_list	= $this->unset_back_to_list;

		$data->validation_url	= $this->getValidationUpdateUrl($state_info->primary_key);
		$data->is_ajax 			= $this->_is_ajax();

		$this->_theme_view('read.php',$data);
		
		$this->_inline_js("var js_date_format = '".$this->js_date_format."';");

		$this->_get_ajax_results();
	}

	//OverRiding the following functions in order to enable the user to set 
	//cdn - FQ url to the application instead of just the ones set in the path

	public function set_css($css_file, $bypass_base = true)
	{
		if($bypass_base)
			$this->css_files[sha1($css_file)] = base_url().$css_file;
		else
			$this->css_files[sha1($css_file)] = $css_file;
	}

	public function set_js($js_file, $bypass_base = true)
	{
		if($bypass_base)
			$this->js_files[sha1($js_file)] = base_url().$js_file;
		else
			$this->js_files[sha1($js_file)] = $js_file;
	}

	public function set_js_lib($js_file, $bypass_base = true)
	{
		if($bypass_base) {
			$this->js_lib_files[sha1($js_file)] = base_url().$js_file;
			$this->js_files[sha1($js_file)] = base_url().$js_file;
		} else {
			$this->js_lib_files[sha1($js_file)] = $js_file;
			$this->js_files[sha1($js_file)] = $js_file;
		}

	}

	public function set_js_config($js_file, $bypass_base = true)
	{
		if($bypass_base) {
			$this->js_config_files[sha1($js_file)] = base_url().$js_file;
			$this->js_files[sha1($js_file)] = base_url().$js_file;
		} else {
			$this->js_config_files[sha1($js_file)] = $js_file;
			$this->js_files[sha1($js_file)] = $js_file;
		}
	}
	
	public function pre_render()
	{
		parent::pre_render();
		if($this->is_grouped)
			$this->finalize_groups();
	}

	protected function finalize_groups() {
		//By default create a default group
		if(array_key_exists('default', $this->groups) == FALSE) {
			$this->groups['default']='default';
			$this->group_fields['default'] = array();
		}
		//Check with all fields - the ones that are not in any group will be in default group
		$add_fields = $this->get_add_fields();
		foreach ($add_fields as $field_num => $field) {
			$_field = $field->field_name;
			$exists = false;
			foreach ($this->group_fields as $gfk=>$_groupfield) {
				if($gfk == 'default')
					continue;
				for($i=0; $i < count($_groupfield); $i++){ 
					if($_field == $_groupfield[$i]) {
						$exists = true;
						break;
					}
					if($exists)
						break;
				}
			} 
			if(!$exists) {
				array_push($this->group_fields['default'], $_field);
			}
		}
	}

	protected function set_default_Model()
	{
		$ci = &get_instance();
		$ci->load->model('Power_grocery_crud_model');

		$this->basic_model = new Power_Grocery_crud_model();
	}

	/**
	 * We re-write this piece of code to allow the default values to be set even while add method
	 * @param  [type] $field_values [description]
	 * @return [type]               [description]
	 */
	protected function get_add_input_fields($field_values = null)
	{
		$fields = $this->get_add_fields();
		$types 	= $this->get_field_types();

		$input_fields = array();

		foreach($fields as $field_num => $field)
		{
			$field_info = $types[$field->field_name];

			$field_value = !empty($field_values) && isset($field_values->{$field->field_name}) ? $field_values->{$field->field_name} : null;

			//Here we add this code - for allowing the library to deal with 
			//default value set by the user in casae of add
			//and will not be disturbing the copy function
			$field_default_values=$this->get_field_default_values($field->field_name);
			if (! empty($field_default_values) and empty($field_value) ){
				$field_value=$this->get_field_default_values($field->field_name);
			}

			if(!isset($this->callback_add_field[$field->field_name]))
			{
				$field_input = $this->get_field_input($field_info, $field_value);
			}
			else
			{
				$field_input = $field_info;
				$field_input->input = call_user_func($this->callback_add_field[$field->field_name], $field_value, null, $field_info);
			}

			switch ($field_info->crud_type) {
				case 'invisible':
					unset($this->add_fields[$field_num]);
					unset($fields[$field_num]);
					continue;
				break;
				case 'hidden':
					$this->add_hidden_fields[] = $field_input;
					unset($this->add_fields[$field_num]);
					unset($fields[$field_num]);
					continue;
				break;
			}

			$input_fields[$field->field_name] = $field_input;
		}

		return $input_fields;
	}

	protected function getCopyUrl($primary_key = null)
	{
		if($primary_key === null)
			return $this->state_url('copy');
		else
			return $this->state_url('copy/'.$primary_key);
	}
}
?>