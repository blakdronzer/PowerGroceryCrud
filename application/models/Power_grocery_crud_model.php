<?php

/**
 * Power Grocery CRUD Model
 * We are extending this model for the very 1 reason - we want to allow users to set values for
 * add fields. This will allow user to set default values for add. And also - allow user to perform
 * the copy functionality.
 *
 * @package    	Power grocery CRUD
 * @author     	Amit Shah <amitbs@gmail.com>
 * @version    	1.1
 */

require_once APPPATH . "models/Grocery_crud_model.php";
class Power_grocery_crud_model extends Grocery_crud_model {
	protected $add_values = null;

	function set_add_value($field_name, $value) {
      $this->add_values[$field_name] = $value;
    }
    function get_add_values() {
        return (object) $this->add_values;
    }
}
